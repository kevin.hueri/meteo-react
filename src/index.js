import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import "./styles/index.scss";

ReactDOM.render(

  //render = rendu
  //StrictMode permet d'avoir moins de bug
  //Toute notre application se déroule dans <App />

  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);