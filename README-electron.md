Electron avec React

Accédez à votre répertoire de travail. Initialisez votre application React à l'aide de 'npx' comme suit. Assurez-vous de donner à votre projet un nom de projet.

```
cd ~/projet

npx create-react-app electron-react-projet
```

La commande npx créera une application React appelée electron-react-projet. Lorsque cette commande est terminée, accédez au répertoire et installez electron.

Vous pouvez le faire dans le terminal comme ceci :

```
cd electron-react-projet

npm i -D electron electron-is-dev
```

La commande a également installé un package npm utile appelé 'electron-is-dev' utilisé pour vérifier si notre application électronique est en développement ou en production. Vous avez utilisé l'indicateur '-D' pour installer electron sous les dépendances de développement.

Ensuite, créez un fichier de configuration pour Electron. Créez-le dans le dossier 'public' où se trouve tout le code HTML qui, dans votre cas, se trouve dans le dossier public appelé electron.js "/electron-react-projet/public/electron.js".

L'étape suivante consiste à ajouter la configuration Electron dans le fichier.

Collez ce code dans le fichier electron.js :

```
const path = require('path');

const { app, BrowserWindow } = require('electron');
const isDev = require('electron-is-dev');

function createWindow() {
  // Creation de la fenêtre du navigateur.
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true,
    },
  });

  // et chargement de l'index html sur l'app.
  // win.loadFile("index.html");
  win.loadURL(
    isDev
      ? 'http://localhost:3000'
      : `file://${path.join(__dirname, '../build/index.html')}`
  );
  // Ouvrir les outils de développement.
  if (isDev) {
    win.webContents.openDevTools({ mode: 'detach' });
  }
}

// Cette méthode sera appelée lorsque Electron aura terminé 
//l'initialisation et sera prêt à créer des fenêtres de navigateur.
// Certaines API ne peuvent être utilisées qu'après que cet événement se soit produit.
app.whenReady().then(createWindow);

// Quittez lorsque toutes les fenêtres sont fermées, sauf sur macOS.
// Là, il est courant que les applications et leur barre de menus 
//restent actives jusqu'à ce que l'utilisateur quitte explicitement
// avec Cmd + Q.
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});
```

Le code crée une instance 'Browserwindow' fournie par electron, qui est utilisée pour restituer le contenu Web. Il charge ensuite le fichier HTML du répertoire dans la fenêtre du navigateur.

Il gère également d'autres événements de fenêtre comme la fermeture lorsque la fenêtre est fermée, le focus lorsque la fenêtre est active, le prêt à afficher lorsque la page Web a été rendue et les états de la fenêtre comme maximiser, minimiser, restaurer.

Le changement important est que vous avez ajouté un fichier HTML personnalisé à lancer. Ce sera dans votre fichier de construction, qui sera la destination en production.


CONFIGURATION DU "package.json"

Electron est maintenant installé, mais vous devez encore apporter quelques modifications au 'package.json' pour synchroniser les versions du navigateur et du bureau. Tout d'abord, mettez à jour le fichier d'entrée du projet.

Dans votre fichier package.json, ajoutez ceci avant vos scripts :

```
"main": "public/electron.js",
```

Ensuite, installez les packages suivants. Ces packages écouteront l'application et, lorsqu'elle se lancera sur le navigateur, elle se lancera à la place en tant qu'application électronique.

```
npm i -D concurrently wait-on
```

Nous permet en même temps d'exécuter plusieurs commandes dans un seul script et l'attente attendra le port 3000, qui est le port CRA par défaut, pour lancer l'application.

L'indicateur BROWSER=none que vous avez transmis dans le script dev empêchera le navigateur de se lancer une fois l'application React compilée avec succès.

Sous scripts dans votre fichier package.json, ajoutez : 

```
"scripts": {
    "start": "react-scripts start",
    "build": "react-scripts build",
    "test": "react-scripts test",
    "eject": "react-scripts eject",
    "dev": "concurrently -k \"BROWSER=none npm start\" \"npm:electron\"",
    "electron": "wait-on tcp:3000 && electron ."
  },
  ```

Vous avez tout mis en place. L'exécution de npm run dev devrait lancer une application électronique. 